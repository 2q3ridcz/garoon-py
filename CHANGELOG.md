# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased] - yyyy-mm-dd

### Added
- BareRestApi.post_schedule_search_available_times
- console_search_available_times

### Changed
- PasswordAuthorization to accept login_name from console.

## [0.3.0] - 2023-04-28
### Added
- PasswordAuthorization to avoid keeping password in class attributes.
- console_copy_schedule_event.

### Changed
- RestApi classes to use PasswordAuthorization instead of password string.
- copy_schedule_event_from_excel output file name to contain timestamp,
    to avoid error of file-already-exists.

## [0.2.0] - 2023-04-23
### Added
- Devcontainer.
- copy_schedule_event_from_excel.

### Changed
- Pytest coverage from statement coverage to branch coverage.
- Disallow in eventType not in ["REGULAR", "ALL_DAY"] in post_schedule_event

## [0.1.0] - 2022-11-05
### Added
- GaroonRestApi.get_schedule_event
- ScheduleUsecase.copy_schedule_event

### Changed
- Divide BareRestApi from GaroonRestApi, to make low level functions (in BareRestApi) simple.

## [0.0.2] - 2022-11-03
### Fixed
- Fixed timezone calculation in GaroonRestApi.get_schedule_events.

### Changed
- Be able to use from anaconda without installing additional packages.
    - Changed lints to pycodestyle, pyflakes, and pylint.
    - Changed source codes to pass lints.

## 0.0.1 - 2022-07-13
### Added
- GaroonRestApi

[Unreleased]: https://gitlab.com/2q3ridcz/garoon-py/-/compare/v0.3.0...main
[0.3.0]: https://gitlab.com/2q3ridcz/garoon-py/-/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/2q3ridcz/garoon-py/-/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/2q3ridcz/garoon-py/-/compare/v0.0.2...v0.1.0
[0.0.2]: https://gitlab.com/2q3ridcz/garoon-py/-/tree/v0.0.2
