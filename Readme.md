# garoon-py

Package to operate on Garoon.

## Reference

- [Garoon REST API 一覧 – cybozu developer network](https://cybozu.dev/ja/garoon/docs/rest-api/list/)
- [パッケージ版Garoon デモサイト](https://onlinedemo2.cybozu.info/scripts/garoon/grn.exe)
