# copy_schedule_event_main

Copies Garoon schedule events as ordered by an excel book.

## Usage

1. Copy `./input_template/copy_schedule.xlsx` to `./input`.
2. Edit `./input/copy_schedule.xlsx`.
3. Run `python ./copy_schedule_event_main.py`.
4. Check results from `./output/copy_schedule_result.xlsx`.

## copy_schedule.xlsx

Check 'description' sheet for how to edit the excel sheet.

## copy_schedule_result.xlsx

Column 'result' indicates if the copy succeeded or not.

- 'Success' means the record's event was copied, and its id is the value of 'new_id'.
    (And its subject, start_date, ... are values of 'new_subject', 'new_start_date, ...)
- 'Skip' means the record is not processed, because 'skip_flg' is not blank.
- Other values are error messages. Something went wrong while processing the record.
