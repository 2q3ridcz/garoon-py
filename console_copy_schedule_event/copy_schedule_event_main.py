"""Sample usage of garoonpy package"""
# pylint: disable=wrong-import-position
import pathlib
import sys

# PKG_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
SCRIPTDIR = pathlib.Path(__file__).parent.resolve()
PKG_PATH = str(SCRIPTDIR.parent.resolve())
if PKG_PATH not in sys.path:
    sys.path.insert(0, PKG_PATH)

import garoonpy
from garoonpy.usecase.schedule_usecase import ScheduleUsecase


INPUTDIR = SCRIPTDIR.joinpath("input")
OUTPUTDIR = SCRIPTDIR.joinpath("output")


def main():
    """Sample usage of garoonpy package"""
    authorization = garoonpy.PasswordAuthorization.create_from_password(
        garoon_url='https://onlinedemo2.cybozu.info/scripts/garoon/grn.exe',
        login_name='sato',
        password='sato',
    )
    grapi = garoonpy.GaroonRestApi(
        authorization=authorization
    )

    input_file = INPUTDIR.joinpath("copy_schedule.xlsx").resolve()
    sheet_name = "copy_schedule"
    usecase = ScheduleUsecase(grapi=grapi)

    print("")
    print("Input:")
    print(input_file)
    print("")

    output_path = usecase.copy_schedule_event_from_excel(
        path=str(input_file),
        sheet_name=sheet_name,
        out_folder=str(OUTPUTDIR.resolve()),
    )
    print("")
    print("Output:")
    print(output_path)
    print("")


if __name__ == '__main__':
    main()
