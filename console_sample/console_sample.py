"""Sample usage of garoonpy package"""
# pylint: disable=wrong-import-position
import datetime
import os
import sys

PKG_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
if PKG_PATH not in sys.path:
    sys.path.insert(0, PKG_PATH)

import garoonpy


def main():
    """Sample usage of garoonpy package"""
    authorization = garoonpy.PasswordAuthorization.create_from_password(
        garoon_url='https://onlinedemo2.cybozu.info/scripts/garoon/grn.exe',
        login_name='sato',
        password='sato',
    )
    grapi = garoonpy.GaroonRestApi(
        authorization=authorization
    )

    print("")
    print("result of get_base_users function (displays first 2):")
    res = grapi.get_base_users()
    print("count = " + str(len(res["users"])))
    print(res["users"][0])
    print(res["users"][1])
    print("")

    print("result of get_schedule_events function (displays first 2):")
    range_start = datetime.datetime(2019, 12, 31, 14, 0, 0)
    range_end = datetime.datetime(2019, 12, 31, 15, 0, 0)
    res = grapi.get_schedule_events(
        range_start=range_start,
        range_end=range_end,
    )
    print("count = " + str(len(res["events"])))
    print("")
    print(res["events"][0])
    print("")
    print(res["events"][1])

    print("")
    print("result of get_schedule_event function:")
    print(grapi.get_schedule_event(event_id="19"))


if __name__ == '__main__':
    main()
