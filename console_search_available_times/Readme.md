# search_available_times_main

Searches users' avaliable times from Garoon schedule (Max 10 times).

## Usage

1. Run `python ./search_available_times_main.py`.
2. Enter dates csv. Date should be in yyyy-mm-dd format. If multiple dates are needed, put comma (,) between the dates.
3. Enter time interval. Time interval should be a number representing minutes.
4. Enter user id csv. User id should be a number. Put comma(,) between the user id.
5. Check available times printed on console. Only first 10 available times will be printed.
