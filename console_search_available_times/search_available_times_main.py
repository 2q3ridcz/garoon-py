"""Sample usage of garoonpy package"""
# pylint: disable=wrong-import-position
import pathlib
import sys
from dateutil import parser

# PKG_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
SCRIPTDIR = pathlib.Path(__file__).parent.resolve()
PKG_PATH = str(SCRIPTDIR.parent.resolve())
if PKG_PATH not in sys.path:
    sys.path.insert(0, PKG_PATH)

import garoonpy


def main():
    """Sample usage of garoonpy package"""
    authorization = garoonpy.PasswordAuthorization.create_from_password(
        garoon_url='https://onlinedemo2.cybozu.info/scripts/garoon/grn.exe',
        login_name='sato',
        password='sato',
    )
    grapi = garoonpy.bare_restapi.bare_restapi.BareRestApi(
        authorization=authorization
    )

    param = {
        "timeRanges": [],
        "timeInterval": 0,
        "attendees": []
    }

    # Get dates from user input and convert to timeRanges
    date_csv = input("Enter dates csv(yyyy-mm-dd):")
    date_list = [date.strip() for date in date_csv.split(',')]
    for date in date_list:
        param["timeRanges"].append(
            {
                "start": date + "T09:00:00+09:00",
                "end": date + "T17:30:00+09:00"
            }
        )

    # Get timeInterval from user input
    time_interval = input("Enter time interval(min):")
    param["timeInterval"] = int(time_interval)

    # Get user id list from user input
    user_id_csv = input("Enter user id csv:")
    user_id_list = [user_id.strip() for user_id in user_id_csv.split(',')]
    for user_id in user_id_list:
        param["attendees"].append(
            {"type": "USER", "id": int(user_id)}
        )

    res = grapi.post_schedule_search_available_times(
        param=param,
    )

    print("")
    print("Response:")
    print(res)
    print("")

    print("")
    print("AvailableTimes (Max 10):")
    for available_time in res['availableTimes']:
        start = parser.parse(available_time["start"]["dateTime"])
        start_str = start.strftime("%Y-%m-%d %H:%M:%S")
        end = parser.parse(available_time["end"]["dateTime"])
        end_str = end.strftime("%Y-%m-%d %H:%M:%S")
        print(f'{start_str} - {end_str}')
    print("")


if __name__ == '__main__':
    main()
