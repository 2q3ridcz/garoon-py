"""Handles Garoon REST API

Not user friendly.
Receives dict and returns dict, without understanding data.

Check the web site for input/output data format.
['Garoon REST API 一覧 – cybozu developer network']
(https://cybozu.dev/ja/garoon/docs/rest-api/list/)
"""
import dataclasses
import json
import typing
import urllib.parse
import requests
from .password_authorization import PasswordAuthorization


@dataclasses.dataclass(frozen=True)
class BareRestApi:
    """Handles Garoon REST API

    Not user friendly.
    Receives dict and returns dict, without understanding data.

    Check the web site for input/output data format.
    ['Garoon REST API 一覧 – cybozu developer network']
    (https://cybozu.dev/ja/garoon/docs/rest-api/list/)
    """
    authorization: PasswordAuthorization

    def headers(self) -> typing.Dict:
        """Returns headers info for Garoon REST API"""
        garoon_url = self.authorization.garoon_url
        auth = self.authorization.auth

        parsed_url = urllib.parse.urlparse(garoon_url)
        subdomain = parsed_url.netloc.split(".")[0]

        return {
            'Host': subdomain + '.cybozu.com:443',
            'Content-Type': 'application/json',
            'X-Cybozu-Authorization': auth,
            'Authorization': 'Basic ' + auth,
        }

    def get_base_users(
            self,
            params: typing.Dict,
    ) -> typing.Dict:
        """Gets from {garoon_url}/api/v1/base/users"""
        garoon_url = self.authorization.garoon_url
        headers = self.headers()

        url = f"{garoon_url}/api/v1/base/users"
        res = requests.get(url=url, headers=headers, params=params, timeout=10)
        res.raise_for_status()
        return res.json()

    def get_schedule_events(
            self,
            params: typing.Dict,
    ) -> typing.Dict:
        """Gets from {garoon_url}/api/v1/schedule/events"""
        garoon_url = self.authorization.garoon_url
        headers = self.headers()

        url = f"{garoon_url}/api/v1/schedule/events"
        res = requests.get(url, headers=headers, params=params, timeout=10)
        res.raise_for_status()
        return res.json()

    def get_schedule_event(
            self,
            event_id: str
    ) -> typing.Dict:
        """Gets from {garoon_url}/api/v1/schedule/events/{event_id}"""
        garoon_url = self.authorization.garoon_url
        headers = self.headers()

        url = f"{garoon_url}/api/v1/schedule/events/{event_id}"
        res = requests.get(url=url, headers=headers, timeout=10)
        res.raise_for_status()
        return res.json()

    def post_schedule_event(
            self,
            event_dict: typing.Dict,
    ) -> typing.Dict:
        """Posts to {garoon_url}/api/v1/schedule/events

        Args:
            event_dict (Dict): Dict of event to post.

        Returns:
            Dict: Dict of registered event.

        Raises:
            HTTPError:
                When something is wrong with event_dict.

            ValueError:
                eventType in event_dict must be one of ["REGULAR", "ALL_DAY"].
                (This check might be removed in future, because api here
                 should work 'without understanding data'.)
        """
        garoon_url = self.authorization.garoon_url
        headers = self.headers()

        # pylint: disable=fixme
        # TODO: Move this check to appropriate class (also change docstring).
        if event_dict["eventType"] not in ["REGULAR", "ALL_DAY"]:
            raise ValueError(" ".join([
                f'event_type is {event_dict["eventType"]}.',
                f'Must be one of {["REGULAR", "ALL_DAY"]}.',
            ]))

        url = f"{garoon_url}/api/v1/schedule/events"
        res = requests.post(
            url=url,
            headers=headers,
            data=json.dumps(event_dict),
            timeout=10
        )
        res.raise_for_status()
        return res.json()

    def post_schedule_search_available_times(
            self,
            param: typing.Dict,
    ) -> typing.Dict:
        """Gets from {garoon_url}/api/v1/schedule/searchAvailableTimes"""
        garoon_url = self.authorization.garoon_url
        headers = self.headers()

        url = f"{garoon_url}/api/v1/schedule/searchAvailableTimes"
        res = requests.post(
            url=url,
            headers=headers,
            data=json.dumps(param),
            timeout=10
        )
        res.raise_for_status()
        return res.json()
