"""Url and authorization data to communicate with garoon server.

[認証 - cybozu developer network]
(https://cybozu.dev/ja/garoon/docs/rest-api/overview/authentication/)
"""
import base64
import dataclasses
import getpass


@dataclasses.dataclass(frozen=True)
class PasswordAuthorization:
    """Url and authorization data to communicate with garoon server.

    For security reason, does not have login_name or password attribute.

    Attributes:
        garoon_url:
            Url of Garoon pointing to grn.exe or grn.cgi. For example:
            - https://onlinedemo2.cybozu.info/scripts/garoon/grn.exe
            - http://onlinedemo2.cybozu.info/cgi-bin/garoon/grn.cgi
        auth: Authorization data to login to Garoon.
    """
    garoon_url: str
    auth: str

    @classmethod
    def create_from_password(
        cls,
        garoon_url: str,
        login_name: str = "",
        password: str = "",
    ) -> "PasswordAuthorization":
        """Creates PasswordAuthorization instance.

        Args:
            garoon_url:
                Url of garoon pointing to grn.exe or grn.cgi. For example:
                - https://onlinedemo2.cybozu.info/scripts/garoon/grn.exe
                - http://onlinedemo2.cybozu.info/cgi-bin/garoon/grn.cgi
            login_name: Login name.
            password: Password.
        """
        if login_name == "":
            name = input("Login name: ")
        else:
            name = login_name

        if password == "":
            pswd = getpass.getpass(prompt="Password: ")
        else:
            pswd = password

        return cls(
            garoon_url=garoon_url,
            auth=base64.b64encode(
                (name + ":" + pswd).encode()
            ).decode()
        )
