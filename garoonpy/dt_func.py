"""Functions to handle datetime."""
# pylint: disable=wrong-import-position
import datetime
import pytz


def to_isoformat_with_timezone(dt: datetime.datetime, tzname: str):
    # pylint: disable=invalid-name
    """Converts datetime to string in isoformat with timezone.

    Ignores argument 'tzname' if datetime has timezone info beforehand"""
    timezone = pytz.timezone(tzname)

    if dt.tzinfo is None:
        return timezone.localize(dt).isoformat()

    return dt.isoformat()
