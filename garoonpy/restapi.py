"""Handles Garoon REST API"""
import datetime
import typing
from garoonpy import dt_func
from garoonpy.bare_restapi.bare_restapi import BareRestApi
from garoonpy.bare_restapi.password_authorization import PasswordAuthorization


class GaroonRestApi:
    """Handles Garoon REST API"""
    def __init__(
            self,
            authorization: PasswordAuthorization,
            timezone: str = "Asia/Tokyo"
    ):
        self.api = BareRestApi(
            authorization=authorization,
        )
        self.timezone = timezone

    def get_base_users(
            self,
            limit: typing.Optional[int] = None,
            offset: typing.Optional[int] = None,
            name: typing.Optional[str] = None,
    ) -> typing.Dict:
        """Gets users info from garoon server"""
        api = self.api

        params = {
            'limit': limit,
            'offset': offset,
            'name': name,
        }
        res = api.get_base_users(
            params=params,
        )
        return res

    def get_schedule_event(
            self,
            event_id: str,
    ) -> typing.Dict:
        """Gets a schedule from garoon server

        The schedule is promised to be unique by event_id.
        """
        api = self.api

        res = api.get_schedule_event(
            event_id=event_id,
        )
        return res

    def get_schedule_events(
            self,
            limit: typing.Optional[int] = None,
            offset: typing.Optional[int] = None,
            fields: typing.Optional[str] = None,
            order_by: typing.Optional[str] = None,
            range_start: typing.Optional[datetime.datetime] = None,
            range_end: typing.Optional[datetime.datetime] = None,
            target: typing.Optional[str] = None,
            target_type: typing.Optional[str] = None,
            keyword: typing.Optional[str] = None,
            exclude_from_search: typing.Optional[str] = None,
    ) -> typing.Dict:
        """Gets schedules from garoon server"""
        # pylint: disable=too-many-arguments, too-many-locals
        api = self.api
        timezone = self.timezone

        if range_start is None:
            range_start_str = None
        else:
            range_start_str = dt_func.to_isoformat_with_timezone(
                dt=range_start,
                tzname=timezone
            )

        if range_end is None:
            range_end_str = None
        else:
            range_end_str = dt_func.to_isoformat_with_timezone(
                dt=range_end,
                tzname=timezone
            )

        params = {
            'limit': limit,
            'offset': offset,
            'fields': fields,
            'orderBy': order_by,
            'rangeStart': range_start_str,
            'rangeEnd': range_end_str,
            'target': target,
            'targetType': target_type,
            'keyword': keyword,
            'excludeFromSearch': exclude_from_search,
        }
        res = api.get_schedule_events(
            params=params,
        )
        return res
