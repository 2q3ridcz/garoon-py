"""Dataclass to hold data of schedule copy excel."""
import dataclasses
import datetime
import typing


@dataclasses.dataclass(frozen=True)
class ScheduleCopyExcelRecord:
    """Dataclass to hold data of schedule copy excel."""
    # pylint: disable=too-many-instance-attributes
    # pylint: disable=redefined-builtin
    # pylint: disable=invalid-name
    id: str
    skip_flg: str
    subject: str
    start_date: str
    start_time: str
    end_date: str
    end_time: str
    notes: str

    def __post_init__(self):
        id = self.id
        start_date = self.start_date
        start_time = self.start_time
        end_date = self.end_date
        end_time = self.end_time

        if id == "":
            raise ValueError("id must not be blank.")
        if start_date == "":
            if start_time != "":
                raise ValueError(
                    "start_time must be blank when start_date is blank."
                )
        if start_date != "":
            if start_time == "":
                raise ValueError(
                    "start_time must not be blank when start_date has value."
                )
        if end_date == "":
            if end_time != "":
                raise ValueError(
                    "end_time must be blank when end_date is blank."
                )
        if end_date != "":
            if end_time == "":
                raise ValueError(
                    "end_time must not be blank when end_date has value."
                )

    @classmethod
    def fields(cls) -> typing.List[str]:
        """Returns field names of this class."""
        return list(cls.__dict__['__dataclass_fields__'].keys())

    def should_be_skipped(self) -> bool:
        """Returns if this record should be skipped."""
        return self.skip_flg != ""

    def event_id(self) -> str:
        """Returns the event_id to copy from."""
        return self.id

    def replace_dict(self) -> typing.Dict[str, typing.Any]:
        """Returns the replace_dict to overwrite from copied event."""
        res = {}
        if self.subject != "":
            res['subject'] = self.subject
        if self.notes != "":
            res['notes'] = self.notes
        if self.start_date != "":
            start_date_str = datetime.datetime.strptime(
                self.start_date, '%Y-%m-%d %H:%M:%S'
            ).strftime('%Y-%m-%d')
            res['start'] = {
                'dateTime': f"{start_date_str}T{self.start_time}+09:00",
                'timeZone': 'Asia/Tokyo',
            }
        if self.end_date != "":
            end_date_str = datetime.datetime.strptime(
                self.end_date, '%Y-%m-%d %H:%M:%S'
            ).strftime('%Y-%m-%d')
            res['end'] = {
                'dateTime': f"{end_date_str}T{self.end_time}+09:00",
                'timeZone': 'Asia/Tokyo',
            }
        return res
