"""Schedule usecase samples using Garoon REST API"""
import datetime
import pathlib
import time
import typing
import pandas as pd
from garoonpy.restapi import GaroonRestApi
from .schedule_copy_excel_record import ScheduleCopyExcelRecord


class ScheduleUsecase:
    # pylint: disable=too-few-public-methods
    """Schedule usecase samples using Garoon REST API"""
    def __init__(
            self,
            grapi: GaroonRestApi,
    ):
        self.grapi = grapi

    def copy_schedule_event(
            self,
            event_id: str,
            replace_dict: typing.Dict
    ) -> typing.Dict:
        """Gets an event, replaces data, and registers as new event"""
        api = self.grapi.api

        event = api.get_schedule_event(event_id=event_id)
        event2 = event.copy()

        for key in replace_dict.keys():
            event2[key] = replace_dict[key]
        res = api.post_schedule_event(event_dict=event2)
        return res

    def copy_schedule_event_from_excel(
            self,
            path: str,
            sheet_name: str,
            out_folder: str,
    ) -> str:
        """Execute copy_schedule_event multiple times using excel.

        Args:
            path (str): Path of the input excel book.
            sheet_name (str): Sheet name of the input excel sheet.
            out_folder (str):
                Path of the folder to create the output excel book.
                Results of each copy are written in this book.
                ("Success", "Skip", or error message.)

        Returns:
            str: Path of the output excel book.
        """
        # pylint: disable=too-many-locals
        # Read excel
        copy_df = pd.read_excel(
            path,
            sheet_name=sheet_name,
            skiprows=1,
            dtype=str,
        )
        copy_df = copy_df.fillna('')

        # Execute copy_schedule_event per record
        fields = ScheduleCopyExcelRecord.fields()
        result_list = []
        new_event_list = []
        # pylint: disable=invalid-name
        rec_seq = 0
        for _, r in copy_df.iterrows():
            rec_seq += 1
            try:
                attribute = {k: str(r[k]) for k in r.keys() if k in fields}
                rec = ScheduleCopyExcelRecord(**attribute)
                if rec.should_be_skipped():
                    result = 'Skip'
                    new_event = ''
                else:
                    event_id = rec.event_id()
                    replace_dict = rec.replace_dict()

                    res = self.copy_schedule_event(
                        event_id=event_id,
                        replace_dict=replace_dict,
                    )
                    result = 'Success'
                    new_event = res
            # pylint: disable=broad-exception-caught
            except BaseException as err:
                msg = f"{type(err).__name__}: {err}"
                print(f'Error at record {rec_seq}:', msg)
                result = msg
                new_event = ''
            result_list.append(result)
            new_event_list.append(new_event)

        # Output result
        write_df = copy_df.copy()
        write_df['result'] = result_list
        write_df["new_id"] = [
            e['id']
            if e != '' else None for e in new_event_list
        ]
        write_df["new_subject"] = [
            e['subject']
            if e != '' else None for e in new_event_list
        ]
        write_df["new_start_date"] = [
            e['start']['dateTime'][0:10]
            if e != '' else None for e in new_event_list
        ]
        write_df["new_start_time"] = [
            e['start']['dateTime'][11:19]
            if e != '' else None for e in new_event_list
        ]
        write_df["new_end_date"] = [
            e['end']['dateTime'][0:10]
            if e != '' else None for e in new_event_list
        ]
        write_df["new_end_time"] = [
            e['end']['dateTime'][11:19]
            if e != '' else None for e in new_event_list
        ]
        write_df["new_notes"] = [
            e['notes']
            if e != '' else None for e in new_event_list
        ]
        # Avoid PermissionError
        in_file = pathlib.Path(path)
        max_retry_count = 60
        interval_seconds = 1
        for _ in range(max_retry_count - 1):
            now = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
            out_file_name = f"{in_file.stem}_result_{now}{in_file.suffix}"
            out_file = pathlib.Path(out_folder).joinpath(out_file_name)
            try:
                write_df.to_excel(out_file, sheet_name=sheet_name, index=False)
                return str(out_file.resolve())
            except PermissionError as err:
                msg = f"{type(err).__name__}: {err}"
                print('Error at output_result:', msg)
            time.sleep(interval_seconds)
        raise RuntimeError(f'Reached max_retry_count({max_retry_count}).')
