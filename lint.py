"""Lints the package"""
import subprocess
import sys

PKG_NAME = "garoonpy"


def lint(pkg_name: str) -> int:
    """Lints a package

    Returns 0 when all checks pass.
    Returns 1 when issues are found.
    """
    exit_code = 0

    for command_prefix in [
            "pycodestyle",
            "pyflakes",
            "pylint",
    ]:
        command = command_prefix + " " + pkg_name
        print("Command:", command)
        res = subprocess.call(command, shell=True)
        if res != 0:
            exit_code = 1
    return exit_code


if __name__ == '__main__':
    sys.exit(lint(pkg_name=PKG_NAME))
