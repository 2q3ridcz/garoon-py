'''Test BareRestApi offline

Does not access to garoon server.
'''


class TestHeader:
    """Test .headers"""
    def test_return_dict(self, api):
        """Test .headers"""
        actual = api.headers()

        assert actual['Host'] == 'dummy.cybozu.com:443'
        assert actual['Content-Type'] == 'application/json'
        assert actual['X-Cybozu-Authorization'] == 'c2F0bzpzYXRv'
        assert actual['Authorization'] == 'Basic c2F0bzpzYXRv'
