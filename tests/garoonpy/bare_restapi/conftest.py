"""Conftest for testing bare_restapi"""
import pytest

import garoonpy
from garoonpy.bare_restapi.bare_restapi import BareRestApi


@pytest.fixture(scope='function', autouse=True)
def api():
    """Yields bare_restapi object"""
    authorization = garoonpy.PasswordAuthorization.create_from_password(
        garoon_url="https://dummy.cybozu.info/scripts/garoon/grn.exe",
        login_name="sato",
        password="sato",
    )
    yield BareRestApi(authorization=authorization)
