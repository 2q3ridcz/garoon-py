'''Test dt_Func'''
import datetime
import garoonpy.dt_func as dt_func


class TestDtToIsoformatWithTimezone:
    def test_return_dict(self):
        # given
        dt = datetime.datetime(2020, 1, 1)
        tzname = "Asia/Tokyo"

        # when
        res = dt_func.to_isoformat_with_timezone(
            dt=dt,
            tzname=tzname,
        )

        assert res == '2020-01-01T00:00:00+09:00'
