import pytest

import garoonpy


@pytest.fixture(scope='function', autouse=True)
def grapi():
    authorization = garoonpy.PasswordAuthorization.create_from_password(
        garoon_url='https://onlinedemo2.cybozu.info/scripts/garoon/grn.exe',
        login_name='sato',
        password='sato',
    )
    yield garoonpy.GaroonRestApi(
        authorization=authorization
    )
