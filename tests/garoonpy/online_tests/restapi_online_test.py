'''Test GaroonRestApi online

Accesses to garoon server.
Can be enabled / disabled by ENABLE_ONLINE_TEST variable.
Fails when garoon server is in maintenance.
'''
import datetime
import pytest


# Set ENABLE_ONLINE_TEST to True to test.
# Aware testcases here access to garoon server when ENABLE_ONLINE_TEST = True.
ENABLE_ONLINE_TEST = False
TEST_ONLINE_CASES = pytest.mark.skipif(
    not ENABLE_ONLINE_TEST,
    reason='access to garoon server should be limited',
)


@TEST_ONLINE_CASES
class TestGetBaseUsers:
    def test_default_params(self, grapi):
        # given
        # when
        res = grapi.get_base_users()
        # then
        assert len(res) == 2

@TEST_ONLINE_CASES
class TestGetScheduleEvents:
    def test_default_params(self, grapi):
        # given
        # when
        res = grapi.get_schedule_events()
        # then
        assert len(res["events"]) == 100

    def test_range_start_str(self, grapi):
        # given
        range_start = datetime.datetime(2020, 1, 1)

        timezone = datetime.timezone(datetime.timedelta(hours=9))
        range_start_tz = range_start.astimezone(timezone)
        # when
        res = grapi.get_schedule_events(
            range_start=range_start
        )
        # then
        assert len(res["events"]) == 100
        for event in res["events"]:
            start = datetime.datetime.fromisoformat(event["start"]["dateTime"])
            assert range_start_tz <= start

    def test_range_end_str(self, grapi):
        '''Test range start and end.

        Garoon REST API returns events which share its period with the range.

        Garoon server returns 2 events when the range is set to
        2019/12/31 14:00-15:00.

        event-1: 2019-12-31T13:30:00+09:00 - 2019-12-31T14:30:00+09:00
        event-2: 2019-12-31T14:00:00+09:00 - 2019-12-31T15:00:00+09:00

        Period of event-2 is completely contained in the range.
        Event-1 is not. It starts before range_start but ends after
        range_start.
        '''
        # given
        range_start = datetime.datetime(2019, 12, 31, 14, 0, 0)
        range_end = datetime.datetime(2019, 12, 31, 15, 0, 0)

        timezone = datetime.timezone(datetime.timedelta(hours=9))
        range_start_tz = range_start.replace(tzinfo=timezone)
        range_end_tz = range_end.replace(tzinfo=timezone)
        # when
        res = grapi.get_schedule_events(
            range_start=range_start,
            range_end=range_end,
        )
        # then
        assert len(res["events"]) == 2
        for event in res["events"]:
            start = datetime.datetime.fromisoformat(event["start"]["dateTime"])
            end = datetime.datetime.fromisoformat(event["end"]["dateTime"])
            assert range_start_tz <= end
            assert start <= range_end_tz
