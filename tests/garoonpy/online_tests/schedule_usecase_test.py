'''Test ScheduleUsecase online

Accesses to garoon server.
Can be enabled / disabled by ENABLE_ONLINE_TEST variable.
Fails when garoon server is in maintenance.
'''
import pathlib
import pytest
import pandas as pd

from garoonpy.usecase.schedule_usecase import ScheduleUsecase


# Set ENABLE_ONLINE_TEST to True to test.
# Aware testcases here access to garoon server when ENABLE_ONLINE_TEST = True.
ENABLE_ONLINE_TEST = False
TEST_ONLINE_CASES = pytest.mark.skipif(
    not ENABLE_ONLINE_TEST,
    reason='access to garoon server should be limited',
)


SCRIPTDIR = pathlib.Path(__file__).parent.resolve()
TESTDATADIR = SCRIPTDIR.joinpath("test-data")
INPUTDIR = TESTDATADIR.joinpath("input")
EXPECTDIR = TESTDATADIR.joinpath("expect")


@TEST_ONLINE_CASES
class TestCopyScheduleEvent:
    # pylint: disable=too-few-public-methods
    """Test .copy_schedule_event"""
    @pytest.mark.parametrize("event_id, replace_dict", [
        # event_id = "19", event_type = "ALL_DAY"
        ("19", {}),
        ("19", {
            'start': {
                'dateTime': '2022-11-07T00:00:00+09:00',
                'timeZone': 'Asia/Tokyo',
            },
            'end': {
                'dateTime': '2022-11-11T23:59:59+09:00',
                'timeZone': 'Asia/Tokyo',
            },
        }),
        # event_id = "63", event_type = "REGULAR"
        ("63", {}),
        ("63", {
            'start': {
                'dateTime': '2022-11-07T00:00:00+09:00',
                'timeZone': 'Asia/Tokyo',
            },
            'end': {
                'dateTime': '2022-11-11T23:59:00+09:00',
                'timeZone': 'Asia/Tokyo',
            },
        }),
    ])
    def test_given_event_type(self, grapi, event_id, replace_dict):
        """Test .copy_schedule_event"""
        # given
        sched = ScheduleUsecase(grapi=grapi)
        # when
        res = sched.copy_schedule_event(
            event_id=event_id,
            replace_dict=replace_dict,
        )
        # then
        event = grapi.get_schedule_event(event_id=event_id)
        keys = set(list(res.keys()) + list(event.keys()))

        assert len(keys) > 5

        for k in keys:
            if k in [
                    "id",
                    "createdAt",
                    "updatedAt",
            ]:
                assert res[k] != event[k]
            elif k in [
                    "creator",
                    "updater",
                    "originalStartTimeZone",
                    "originalEndTimeZone",
            ]:
                continue
            elif k in replace_dict.keys():
                assert res[k] != event[k]
                assert res[k] == replace_dict[k]
            else:
                assert res[k] == event[k]


@TEST_ONLINE_CASES
class TestCopyScheduleEventFromExcel:
    def test_operational(self, tmp_path, grapi):
        """Works when various patterns are in one excel."""
        # given
        input_file = INPUTDIR.joinpath("copy_schedule.xlsx").resolve()
        sheet_name = "copy_schedule"
        usecase = ScheduleUsecase(grapi=grapi)
        # when
        out_path = usecase.copy_schedule_event_from_excel(
            path=str(input_file),
            sheet_name=sheet_name,
            out_folder=str(tmp_path.resolve()),
        )
        # then
        out_file = pathlib.Path(out_path)
        assert out_file.parent == tmp_path
        assert out_file.name.startswith(input_file.stem)
        assert out_file.suffix == input_file.suffix

        expect_file = EXPECTDIR.joinpath("copy_schedule_result.xlsx").resolve()
        expect_df = pd.read_excel(expect_file, sheet_name=sheet_name, dtype=str)
        output_df = pd.read_excel(out_file, sheet_name=sheet_name, dtype=str)
        # Replace values of 'new_id'. 'new_id' changes every time.
        expect_df['new_id'] = expect_df['new_id'].notna()
        output_df['new_id'] = output_df['new_id'].notna()
        pd.testing.assert_frame_equal(output_df, expect_df)
