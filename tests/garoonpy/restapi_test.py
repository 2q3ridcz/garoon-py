'''Test GaroonRestApi offline

Does not access to garoon server.
Tests if GaroonRestApi sets the correct arguments to requests.get.
To do this, requests.get is mocked to return arguments (instead of response).
'''
import datetime
from unittest.mock import patch


class TestGetBaseUsers:
    @patch("garoonpy.bare_restapi.bare_restapi.requests")
    def test_default_params(self, mock_requests, grapi):
        # given
        # when
        grapi.get_base_users()
        # then
        kwargs = mock_requests.get.call_args[1]
        assert kwargs["params"] == {
            'limit': None,
            'offset': None,
            'name': None,
        }

class TestGetScheduleEvent:
    @patch("garoonpy.bare_restapi.bare_restapi.requests")
    def test_id(self, mock_requests, grapi):
        # given
        event_id = "10"
        # when
        grapi.get_schedule_event(event_id=event_id)
        # then
        kwargs = mock_requests.get.call_args[1]
        assert kwargs["url"].split("/")[-1] == event_id
        assert ("params" in kwargs.keys()) is False


class TestGetScheduleEvents:
    @patch("garoonpy.bare_restapi.bare_restapi.requests")
    def test_default_params(self, mock_requests, grapi):
        # given
        # when
        grapi.get_schedule_events()
        # then
        kwargs = mock_requests.get.call_args[1]
        assert kwargs["params"] == {
            'limit': None,
            'offset': None,
            'fields': None,
            'orderBy': None,
            'rangeStart': None,
            'rangeEnd': None,
            'target': None,
            'targetType': None,
            'keyword': None,
            'excludeFromSearch': None,
        }

    @patch("garoonpy.bare_restapi.bare_restapi.requests")
    def test_range_start_str(self, mock_requests, grapi):
        # given
        # when
        grapi.get_schedule_events(
            range_start=datetime.datetime(2020, 1, 1)
        )
        # then
        kwargs = mock_requests.get.call_args[1]
        assert kwargs["params"] == {
            'limit': None,
            'offset': None,
            'fields': None,
            'orderBy': None,
            'rangeStart': '2020-01-01T00:00:00+09:00',
            'rangeEnd': None,
            'target': None,
            'targetType': None,
            'keyword': None,
            'excludeFromSearch': None,
        }

    @patch("garoonpy.bare_restapi.bare_restapi.requests")
    def test_range_end_str(self, mock_requests, grapi):
        # given
        # when
        grapi.get_schedule_events(
            range_start=datetime.datetime(2020, 1, 1, 12, 0, 0),
            range_end=datetime.datetime(2020, 12, 21),
        )
        # then
        kwargs = mock_requests.get.call_args[1]
        assert kwargs["params"] == {
            'limit': None,
            'offset': None,
            'fields': None,
            'orderBy': None,
            'rangeStart': '2020-01-01T12:00:00+09:00',
            'rangeEnd': '2020-12-21T00:00:00+09:00',
            'target': None,
            'targetType': None,
            'keyword': None,
            'excludeFromSearch': None,
        }
